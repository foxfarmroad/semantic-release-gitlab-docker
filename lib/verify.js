const execa = require('execa')

module.exports = async (pluginConfig, { logger }) => {
  for (const envVar of ['CI_BUILD_TOKEN']) {
    if (!process.env[envVar]) {
      throw new Error(`Environment variable ${envVar} is not set`)
    }
  }
  try {
    await execa(
      'docker',
      [
        'login',
        '-u=gitlab-ci-token',
        '-p=' + process.env.CI_BUILD_TOKEN,
        'registry.gitlab.com',
      ],
      {
        stdio: 'inherit',
      }
    )
  } catch (err) {
    throw new Error('Gitlab registry login failed')
  }
}
